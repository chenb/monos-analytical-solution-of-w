#include "TH1.h"
#include "TH1D.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphQQ.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TRandom.h"

#include <TMatrixD.h>

using namespace TMath;

void rotateNu(TLorentzVector& neu){
    neu.RotateY(1.45);
}

void test2(){
    TLorentzVector n;
    n.SetPtEtaPhiM(8, 1.2, 1.5, 10);
    cout << n.Px() << endl;
    rotateNu(n);
    cout << n.Px() << endl;
}
















