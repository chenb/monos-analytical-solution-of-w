#include "TFile.h"
#include "TTree.h"
#include <iostream>
#include <vector>
#include <cmath>

#include <TMatrixD.h>

#include "Math/Functor.h"
#include "Math/BrentMinimizer1D.h"

using namespace std;
using namespace TMath;

const Float_t mW = 80.379; //GeV
const Float_t LUMI = 138.9;
const TString dir = "/hep300/data-shared/MonoS/SWWlep002_ttrees_flattened_200714/SR1L/";

class msFunc{
    public:
        double thetawl;
        TLorentzVector lepton;
        TLorentzVector Whad;
        
        double term1;
        double term2;
        double term3;

        double operator() (double x){
            thetawl = lepton.Angle(Whad.Vect());
            term1 = lepton.E() + pow(mW, 2)/(2*lepton.E()*(1 - x)) + Whad.E();
            term2 = Whad.P()*sin(thetawl) + (sqrt(1 - pow(x, 2))*pow(mW,2))/(2*lepton.E()*(1 - x));
            term3 = lepton.E() + Whad.P()*cos(thetawl) + (pow(mW,2)*x)/(2*lepton.E()*(1 - x));
            return pow(term1, 2) - pow(term2, 2) - pow(term3, 2);
        }
};

Float_t calc_dmMet(Float_t coslv, TLorentzVector lep, TLorentzVector MET);

void calc_DM_MET_v2(){
    Int_t nBG = 3;
    Int_t applyWeighting = 0;
    Float_t evtWeightCut = 5000;
    
    vector<TString> fnamev_sig;
    vector<TString> fnamev_wjets;
    vector<TString> fnamev_dib;
    vector<TString> fnamev_tt;
    
    fnamev_sig.push_back("monoSww_all_zp1000_dm200_dh160");
    fnamev_sig.push_back("monoSww_all_zp2500_dm200_dh160");
    fnamev_sig.push_back("monoSww_semilep_zp1000_dm200_dh185");
    fnamev_sig.push_back("monoSww_semilep_zp1000_dm200_dh210");
    fnamev_sig.push_back("monoSww_semilep_zp1000_dm200_dh235");
    fnamev_sig.push_back("monoSww_semilep_zp1000_dm200_dh260");
    fnamev_sig.push_back("monoSww_semilep_zp1000_dm200_dh285");
    fnamev_sig.push_back("monoSww_semilep_zp1000_dm200_dh310");
    fnamev_sig.push_back("monoSww_semilep_zp1700_dm200_dh160");
    fnamev_sig.push_back("monoSww_semilep_zp1700_dm200_dh185");
    fnamev_sig.push_back("monoSww_semilep_zp1700_dm200_dh210");
    fnamev_sig.push_back("monoSww_semilep_zp1700_dm200_dh235");
    fnamev_sig.push_back("monoSww_semilep_zp1700_dm200_dh260");
    fnamev_sig.push_back("monoSww_semilep_zp2100_dm200_dh210");
    fnamev_sig.push_back("monoSww_semilep_zp500_dm200_dh160");
    fnamev_sig.push_back("monoSww_semilep_zp500_dm200_dh185");
    fnamev_sig.push_back("monoSww_semilep_zp500_dm200_dh210");
    fnamev_sig.push_back("monoSww_semilep_zp500_dm200_dh235");
    fnamev_sig.push_back("monoSww_semilep_zp500_dm200_dh260");
    fnamev_sig.push_back("monoSww_semilep_zp500_dm200_dh285");
    fnamev_sig.push_back("monoSww_semilep_zp500_dm200_dh310"); 
 
    fnamev_wjets.push_back("Wenu_cl");
    fnamev_wjets.push_back("Wenu_hf");
    fnamev_wjets.push_back("Wenu_hpt");
    fnamev_wjets.push_back("Wenu_l");
    fnamev_wjets.push_back("Wmunu_cl");
    fnamev_wjets.push_back("Wmunu_hf");
    fnamev_wjets.push_back("Wmunu_hpt");
    fnamev_wjets.push_back("Wmunu_l");
    fnamev_wjets.push_back("Wtaunu_cl");
    fnamev_wjets.push_back("Wtaunu_hf");
    fnamev_wjets.push_back("Wtaunu_hpt");
    fnamev_wjets.push_back("Wtaunu_l");

    fnamev_dib.push_back("WW");
    fnamev_dib.push_back("WZ");
    fnamev_dib.push_back("ZZ");
    
    fnamev_tt.push_back("ttbar");
    fnamev_tt.push_back("stops");
    fnamev_tt.push_back("stopt");
    fnamev_tt.push_back("stopWt");
 
    TChain* bg_chain[nBG];
    TString bg_names[] = {"w+jets", "ttbar", "WW, WZ, ZZ"};

    for(int i = 0; i < nBG; i++){
        bg_chain[i] = new TChain();
    }

    for(int i = 0; i < fnamev_wjets.size(); i++){
        bg_chain[0]->Add(dir + fnamev_wjets[i] + ".root" + "/" + fnamev_wjets[i] + "_Nominal");
    }
    
    for(int i = 0; i < fnamev_tt.size(); i++){
        bg_chain[1]->Add(dir + fnamev_tt[i] + ".root" + "/" + fnamev_tt[i] + "_Nominal");
    }
    
    for(int i = 0; i < fnamev_dib.size(); i++){
        bg_chain[2]->Add(dir + fnamev_dib[i] + ".root" + "/" + fnamev_dib[i] + "_Nominal");
    }

    //==========================================================================================================
    
    Float_t l_pT;
    Float_t l_phi;
    Float_t l_eta;
    Float_t l_m;

    Int_t N_FatJets;
    Int_t N_BTrackJets;
    Float_t mT_lep_met;
    Float_t MetTST_met;
    Float_t MetTST_phi;
    Float_t FatJet_m0;
    Float_t MetTST_Significance;
    Double_t dR_lep_FatJets;
    Float_t FatJet_D20;

    Int_t N_BJets_04;
    Int_t N_Jets04;
    Float_t WCand_m;
    Float_t WCand_pt;
    Float_t dRWl;

    Float_t FatJet_pt0;
    Float_t FatJet_eta0;
    Float_t FatJet_phi0;

    Float_t WCand_eta;
    Float_t WCand_phi;
    
    Double_t NormWeight;
    Double_t JetWeightJVT;
    Double_t MuoWeight;
    Double_t EleWeight;
    Double_t prwWeight;

    TLorentzVector lepton;
    TLorentzVector neutrino;
    TLorentzVector Whad;
    TLorentzVector MET;

    //==========================================================================================================
    
    TH1F* dm_bg_sum[nBG];
    TH1F* dm_bg_mgd[nBG];
    TH1F* dm_bg_res[nBG];
    
    THStack* bg_sum_stack = new THStack("bg_sum_stack", "");
    THStack* bg_mgd_stack = new THStack("bg_mgd_stack", "");
    THStack* bg_res_stack = new THStack("bg_res_stack", "");

    Int_t histColors[] = {46, 9, 30};
 
    //==========================================================================================================
    
    ofstream scaleFile;
    scaleFile.open("./ScaleFactors/dm_scales_factors.txt", ios_base::app);

    //==========================================================================================================

    for(int i = 0; i < nBG; i++){
        TTree* bgtree = bg_chain[i];
        
        dm_bg_sum[i] = new TH1F((TString) i, "", 50, 0, 750);
        dm_bg_mgd[i] = new TH1F((TString) i, "", 50, 0, 750);
        dm_bg_res[i] = new TH1F((TString) i, "", 50, 0, 750);

        bgtree->SetBranchAddress("Lepton_pt", &l_pT);
        bgtree->SetBranchAddress("Lepton_phi", &l_phi);
        bgtree->SetBranchAddress("Lepton_eta", &l_eta);
        bgtree->SetBranchAddress("Lepton_m", &l_m);

        bgtree->SetBranchAddress("N_FatJets", &N_FatJets);
        bgtree->SetBranchAddress("N_BTrackJets", &N_BTrackJets);
        bgtree->SetBranchAddress("mT_lep_met", &mT_lep_met);
        bgtree->SetBranchAddress("MetTST_met", &MetTST_met);
        bgtree->SetBranchAddress("MetTST_phi", &MetTST_phi);
        bgtree->SetBranchAddress("FatJet_m0", &FatJet_m0);
        bgtree->SetBranchAddress("MetTST_Significance", &MetTST_Significance);
        bgtree->SetBranchAddress("dR_lep_FatJets", &dR_lep_FatJets);
        bgtree->SetBranchAddress("FatJet_D20", &FatJet_D20);
        bgtree->SetBranchAddress("N_BJets_04", &N_BJets_04);
        bgtree->SetBranchAddress("N_Jets04", &N_Jets04);
        bgtree->SetBranchAddress("WCand_m", &WCand_m);
        bgtree->SetBranchAddress("WCand_pt", &WCand_pt);
        bgtree->SetBranchAddress("dRWl", &dRWl);

        bgtree->SetBranchAddress("FatJet_pt0", &FatJet_pt0);
        bgtree->SetBranchAddress("FatJet_eta0", &FatJet_eta0);
        bgtree->SetBranchAddress("FatJet_phi0", &FatJet_phi0);

        bgtree->SetBranchAddress("WCand_eta", &WCand_eta);
        bgtree->SetBranchAddress("WCand_phi", &WCand_phi);

        bgtree->SetBranchAddress("NormWeight", &NormWeight);
        bgtree->SetBranchAddress("JetWeightJVT", &JetWeightJVT);
        bgtree->SetBranchAddress("MuoWeight", &MuoWeight);
        bgtree->SetBranchAddress("EleWeight", &EleWeight);
        bgtree->SetBranchAddress("prwWeight", &prwWeight);

        Int_t nEntries = bgtree->GetEntries();

        for(int j = 0; j < nEntries; j++){
            bgtree->GetEntry(j);
            
            Float_t evtWeight = 1.0;

            if(applyWeighting){
                evtWeight *= NormWeight * JetWeightJVT * MuoWeight * EleWeight * prwWeight * LUMI;
            }

            if(evtWeight > evtWeightCut){
                continue;
            }

            msFunc mf;
            ROOT::Math::Functor1D func(mf);
            ROOT::Math::BrentMinimizer1D bm;
            bm.SetFunction(func, -1, 0.999);

            if(j%100000 == 0){
                cout << "Finished Entry: " << j << endl;
            }
            
            if(N_FatJets > 0 &&
               N_BTrackJets < 1 &&
               mT_lep_met > 150 &&
               MetTST_met > 300 &&
               FatJet_m0 > 60 &&
               FatJet_m0 < 100 &&
               MetTST_Significance > 15 &&
               dR_lep_FatJets < 1.6 &&
               FatJet_D20 < 1.4){

                lepton.SetPtEtaPhiM(l_pT, l_eta, l_phi, l_m);
                Whad.SetPtEtaPhiM(FatJet_pt0, FatJet_eta0, FatJet_phi0, FatJet_m0);
                MET.SetPtEtaPhiM(MetTST_met, 0, MetTST_phi, 0);

                mf.lepton = lepton;
                mf.Whad = Whad;
                func = mf;
                bm.Minimize(1000, 0.001, 0.0001);

                Float_t coslv = bm.XMinimum();
                Float_t dmMet =  calc_dmMet(coslv, lepton, MET);
                
                dm_bg_mgd[i]->Fill(dmMet, evtWeight);
                dm_bg_sum[i]->Fill(dmMet, evtWeight);

            }else if(MetTST_met >= 150 &&
                     N_BJets_04 == 0 &&
                     N_Jets04 >= 2 &&
                     WCand_m > 60 &&
                     WCand_m < 100 &&
                     mT_lep_met >= 200 &&
                     WCand_pt >= 100 &&
                     MetTST_Significance >= 13 &&
                     dRWl <= 1){

                lepton.SetPtEtaPhiM(l_pT, l_eta, l_phi, l_m);
                Whad.SetPtEtaPhiM(WCand_pt, WCand_eta, WCand_phi, WCand_m);
                MET.SetPtEtaPhiM(MetTST_met, 0, MetTST_phi, 0);

                mf.lepton = lepton;
                mf.Whad = Whad;
                func = mf;
                bm.Minimize(1000, 0.001, 0.0001);

                Float_t coslv = bm.XMinimum();
                Float_t dmMet =  calc_dmMet(coslv, lepton, MET);
 
                dm_bg_res[i]->Fill(dmMet, evtWeight);
                dm_bg_sum[i]->Fill(dmMet, evtWeight);

            }else{
                continue;
            } 
        }
        
        dm_bg_sum[i]->SetFillColor(histColors[i]);
        dm_bg_sum[i]->SetLineColor(histColors[i]);
        bg_sum_stack->Add(dm_bg_sum[i]);

        dm_bg_mgd[i]->SetFillColor(histColors[i]);
        dm_bg_mgd[i]->SetLineColor(histColors[i]);
        bg_mgd_stack->Add(dm_bg_mgd[i]);

        dm_bg_res[i]->SetFillColor(histColors[i]);
        dm_bg_res[i]->SetLineColor(histColors[i]);
        bg_res_stack->Add(dm_bg_res[i]);

        cout << "Finished background: " << bg_names[i] << endl;
    }

    for(int i = 0; i < fnamev_sig.size(); i++){
        TH1F* dm_sig_res = new TH1F("dm_sig_res", "", 50, 0, 750);
        TH1F* dm_sig_mgd = new TH1F("dm_sig_mgd", "", 50, 0, 750);
        TH1F* dm_sig_sum = new TH1F("dm_sig_sum", "", 50, 0, 750);

        TFile* f = new TFile(dir + fnamev_sig[i] + ".root");
        TTree *sigTree = (TTree*)f->Get(fnamev_sig[i] + "_Nominal");
        
        sigTree->SetBranchAddress("Lepton_pt", &l_pT);
        sigTree->SetBranchAddress("Lepton_phi", &l_phi);
        sigTree->SetBranchAddress("Lepton_eta", &l_eta);
        sigTree->SetBranchAddress("Lepton_m", &l_m);

        sigTree->SetBranchAddress("N_FatJets", &N_FatJets);
        sigTree->SetBranchAddress("N_BTrackJets", &N_BTrackJets);
        sigTree->SetBranchAddress("mT_lep_met", &mT_lep_met);
        sigTree->SetBranchAddress("MetTST_met", &MetTST_met);
        sigTree->SetBranchAddress("MetTST_phi", &MetTST_phi);
        sigTree->SetBranchAddress("FatJet_m0", &FatJet_m0);
        sigTree->SetBranchAddress("MetTST_Significance", &MetTST_Significance);
        sigTree->SetBranchAddress("dR_lep_FatJets", &dR_lep_FatJets);
        sigTree->SetBranchAddress("FatJet_D20", &FatJet_D20);
        sigTree->SetBranchAddress("N_BJets_04", &N_BJets_04);
        sigTree->SetBranchAddress("N_Jets04", &N_Jets04);
        sigTree->SetBranchAddress("WCand_m", &WCand_m);
        sigTree->SetBranchAddress("WCand_pt", &WCand_pt);
        sigTree->SetBranchAddress("dRWl", &dRWl);

        sigTree->SetBranchAddress("FatJet_pt0", &FatJet_pt0);
        sigTree->SetBranchAddress("FatJet_eta0", &FatJet_eta0);
        sigTree->SetBranchAddress("FatJet_phi0", &FatJet_phi0);

        sigTree->SetBranchAddress("WCand_eta", &WCand_eta);
        sigTree->SetBranchAddress("WCand_phi", &WCand_phi);

        sigTree->SetBranchAddress("NormWeight", &NormWeight);
        sigTree->SetBranchAddress("JetWeightJVT", &JetWeightJVT);
        sigTree->SetBranchAddress("MuoWeight", &MuoWeight);
        sigTree->SetBranchAddress("EleWeight", &EleWeight);
        sigTree->SetBranchAddress("prwWeight", &prwWeight);

        Int_t nEntries = sigTree->GetEntries();
        
        for(int j = 0; j < nEntries; j++){
            sigTree->GetEntry(j);

            Float_t evtWeight = 1.0;
            if(applyWeighting){
                evtWeight *= NormWeight * JetWeightJVT * MuoWeight * EleWeight * prwWeight * LUMI;
            }

            if(evtWeight > evtWeightCut){
                continue;
            }

            msFunc mf;
            ROOT::Math::Functor1D func(mf);
            ROOT::Math::BrentMinimizer1D bm;
            bm.SetFunction(func, -1, 0.999);
 
            if(N_FatJets > 0 &&
               N_BTrackJets < 1 &&
               mT_lep_met > 150 &&
               MetTST_met > 300 &&
               FatJet_m0 > 60 &&
               FatJet_m0 < 100 &&
               MetTST_Significance > 15 &&
               dR_lep_FatJets < 1.6 &&
               FatJet_D20 < 1.4){

                lepton.SetPtEtaPhiM(l_pT, l_eta, l_phi, l_m);
                Whad.SetPtEtaPhiM(FatJet_pt0, FatJet_eta0, FatJet_phi0, FatJet_m0);
                MET.SetPtEtaPhiM(MetTST_met, 0, MetTST_phi, 0);

                mf.lepton = lepton;
                mf.Whad = Whad;
                func = mf;
                bm.Minimize(1000, 0.001, 0.0001);

                Float_t coslv = bm.XMinimum();
                Float_t dmMet =  calc_dmMet(coslv, lepton, MET);

                dm_sig_mgd->Fill(dmMet, evtWeight);
                dm_sig_sum->Fill(dmMet, evtWeight);

            }else if(MetTST_met >= 150 &&
                     N_BJets_04 == 0 &&
                     N_Jets04 >= 2 &&
                     WCand_m > 60 &&
                     WCand_m < 100 &&
                     mT_lep_met >= 200 &&
                     WCand_pt >= 100 &&
                     MetTST_Significance >= 13 &&
                     dRWl <= 1){

                lepton.SetPtEtaPhiM(l_pT, l_eta, l_phi, l_m);
                Whad.SetPtEtaPhiM(WCand_pt, WCand_eta, WCand_phi, WCand_m);
                MET.SetPtEtaPhiM(MetTST_met, 0, MetTST_phi, 0);

                mf.lepton = lepton;
                mf.Whad = Whad;
                func = mf;
                bm.Minimize(1000, 0.001, 0.0001);

                Float_t coslv = bm.XMinimum();
                Float_t dmMet =  calc_dmMet(coslv, lepton, MET);

                dm_sig_res->Fill(dmMet, evtWeight);
                dm_sig_sum->Fill(dmMet, evtWeight);

            }else{
                continue;
            } 
        }

        cout << "Finished file: " << fnamev_sig[i] << endl;

        TText* t = new TText(0.5, 0.93, fnamev_sig[i]);
        t->SetNDC();
        t->SetTextAlign(22);
        t->SetTextFont(43);
        t->SetTextSize(30);

        TCanvas* c = new TCanvas("c", "Signal", 2000, 800);
        c->Divide(3,1);

        c->cd(1);
        TLegend* leg_sum = new TLegend(0.15, 0.7, 0.45, 0.85);
        leg_sum->AddEntry(dm_bg_sum[0], "W+jets [sum]", "F");
        leg_sum->AddEntry(dm_bg_sum[1], "ttbar [sum]", "F");
        leg_sum->AddEntry(dm_bg_sum[2], "WW, WZ, ZZ [sum]", "F");
        leg_sum->AddEntry(dm_sig_sum, "Signal [sum]", "P");
        leg_sum->SetBorderSize(0);
        leg_sum->SetFillStyle(0);

        bg_sum_stack->SetMinimum(0);
        bg_sum_stack->Draw("HIST");
        gPad->Update();

        bg_sum_stack->GetXaxis()->SetTitle("DM MET [GeV]");

        Float_t rightmax1 = 1.1*dm_sig_sum->GetMaximum();
        Float_t scale1 = gPad->GetUymax()/rightmax1;
        dm_sig_sum->SetMarkerStyle(22);
        dm_sig_sum->SetMarkerColor(1);
        dm_sig_sum->Scale(scale1);
        dm_sig_sum->Draw("HIST P SAME");
        leg_sum->Draw();
        gPad->Modified();

        c->cd(2);

        TLegend* leg_mgd = new TLegend(0.15, 0.7, 0.45, 0.85);
        leg_mgd->AddEntry(dm_bg_mgd[0], "W+jets [mgd]", "F");
        leg_mgd->AddEntry(dm_bg_mgd[1], "ttbar [mgd]", "F");
        leg_mgd->AddEntry(dm_bg_mgd[2], "WW, WZ, ZZ [mgd]", "F");
        leg_mgd->AddEntry(dm_sig_mgd, "Signal [mgd]", "P");
        leg_mgd->SetBorderSize(0);
        leg_mgd->SetFillStyle(0);

        bg_mgd_stack->SetMinimum(0);
        bg_mgd_stack->Draw("HIST");
        gPad->Update();

        t->Draw();
        bg_mgd_stack->GetXaxis()->SetTitle("DM MET [GeV]");

        Float_t rightmax2 = 1.1*dm_sig_mgd->GetMaximum();
        Float_t scale2 = gPad->GetUymax()/rightmax2;
        dm_sig_mgd->SetMarkerStyle(3);
        dm_sig_mgd->SetMarkerColor(1);
        dm_sig_mgd->Scale(scale2);
        dm_sig_mgd->Draw("HIST P SAME");
        leg_mgd->Draw();
        gPad->Modified();

        c->cd(3);
        TLegend* leg_res = new TLegend(0.15, 0.7, 0.45, 0.85);
        leg_res->AddEntry(dm_bg_res[0], "W+jets [res]", "F");
        leg_res->AddEntry(dm_bg_res[1], "ttbar [res]", "F");
        leg_res->AddEntry(dm_bg_res[2], "WW, WZ, ZZ [res]", "F");
        leg_res->AddEntry(dm_sig_res, "Signal [res]", "P");
        leg_res->SetBorderSize(0);
        leg_res->SetFillStyle(0);

        bg_res_stack->SetMinimum(0);
        bg_res_stack->Draw("HIST");
        gPad->Update();

        bg_res_stack->GetXaxis()->SetTitle("DM MET [GeV]");

        Float_t rightmax3 = 1.1*dm_sig_res->GetMaximum();
        Float_t scale3 = gPad->GetUymax()/rightmax3;
        dm_sig_res->SetMarkerStyle(8);
        dm_sig_res->SetMarkerColor(1);
        dm_sig_res->Scale(scale3);
        dm_sig_res->Draw("HIST P SAME");
        leg_res->Draw();
        gPad->Modified();
        
        scaleFile << fnamev_sig[i] + "\n";
        scaleFile << "SUM SCALE = " << scale1 << "\n";
        scaleFile << "MGD SCALE = " << scale2 << "\n";
        scaleFile << "RES SCALE = " << scale3 << "\n";

        if(i == 0){
            c->Print("dmMET_AllSignal_AllBG_UW.pdf(", "pdf");
        }else if(i == fnamev_sig.size()-1){
            c->Print("dmMET_AllSignal_AllBG_UW.pdf)", "pdf");
        }else{
            c->Print("dmMET_AllSignal_AllBG_UW.pdf", "pdf");
        }
    }
}

Float_t calc_dmMet(Float_t coslv, TLorentzVector lep, TLorentzVector MET){
    Float_t sinlv = sqrt(1 - pow(coslv, 2));
    Float_t nuE = pow(mW,2)/(2*lep.E()*(1 - coslv));

    TLorentzVector nu;
    nu.SetPxPyPzE(nuE*sinlv, 0, nuE*coslv, nuE);
    nu.RotateY(lep.Theta()); 
    nu.RotateZ(lep.Phi());

    Float_t dmMet = sqrt(pow(MET.Px() - nu.Px(), 2) + pow(MET.Py() - nu.Py(), 2));

    return dmMet;
}

